//connect to the admin database
var db = connect("admin");
db.auth("admin", [PASSWORD]);

//create the FaceSecurity Database and user
db = db.getSiblingDB("FaceSecurity");
db.createUser({user: "facesecurity", pwd: "facesecurity", roles: [{role: "readWrite", db: "FaceSecurity"}]});

//create the suspects collection
db.createCollection("suspects");
db.suspects.createIndex({"label": 1}, {unique: true});
db.suspects.insert({
                    "label"   : null,
                    "message" : "nobody",
                    "date"    : new Date(),
                    "photos"  : [""]
                   });

//create the devices collection
db.createCollection("devices");
db.devices.createIndex({"ip": 1}, {unique: true});
db.devices.createIndex({"name": 1}, {unique: true});
db.devices.insert({"ip": null, "name": null, "type": "", "key": ""})
