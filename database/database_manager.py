#imports
import datetime
import pymongo as pm
import threading

#Exception Class for database errors
#@author Luis Miguel Mejía Suárez
class DatabaseError(Exception):
  pass

#Database Manager
#This class manage all the operations over the database
#@author Luis Miguel Mejía Suárez
class DatabaseManager:
  #open a connection with the database
  #@param host (str) the hostname or ip address where is located the database
  #@param port (int) the port number which the database is listening
  #@raises DatabaseError if can't open a connection with the database
  def __init__(self, host, port):
    try:
      self.client = pm.MongoClient(host, port)
      self.db     = self.client["FaceSecurity"]
      self.db.authenticate("facesecurity", "facesecurity")
      self.suspects = self.db["suspects"]
      self.devices  = self.db["devices"]
      #lock to make the class thread safe
      self.lock = threading.Lock()
    except Exception as e:
      raise DatabaseError("Something went wrong when connection to the database\n" + str(e))

  #register a new device in the database
  #@param ip (str) the ip address of the device to register, must be unique
  #@param name (str) the name to assign to the device, must be unique
  #@paran key (str) the encryption key for this device
  #@paran device (str) determine if the device is a client or a camera
  #@return True if the insertion process worked
  #@raises DatabaseError if device is different from client or camera
  #@raises DatabaseError if the ip or name was already used
  #@raises DatabaseError if something fail while inserting into database
  def register_device(self, ip, name, key, device):
    #thread safe
    with self.lock:
      if device == "client" or device == "camera":
        try:
          self.devices.insert_one({"ip": ip, "name": name, "type": device, "key": key})
        except pm.errors.DuplicateKeyError:
          raise DatabaseError("The ip '" + ip + "' or name '" + name + "' was already registered")
        except Exception as e:
          raise DatabaseError("Something went wrong with the database\n" + str(e))
      else:
        raise DatabaseError("Bad device: " +  device)

      return True

  #register a new client in the database
  #@param ip (str) the ip address of the client to register, must be unique
  #@param name (str) the name to assign to the client, must be unique
  #@paran key (str) the encryption key for this device
  #@return True if the insertion process worked
  #@raises DatabaseError if the ip or name was already used
  #@raises DatabaseError if something fail while inserting into database
  def register_client(self, ip, name, key):
    return self.register_device(ip, name, key, "client")

  #register a new camera in the database
  #@param ip (str) the ip address of the camera to register, must be unique
  #@param name (str) the name to assign to the camera, must be unique
  #@paran key (str) the encryption key for this device
  #@return True if the insertion process worked
  #@raises DatabaseError if the ip or name was already used
  #@raises DatabaseError if something fail while inserting into database
  def register_camera(self, ip, name, key):
    return self.register_device(ip, name, key, "camera")

  #get all registered devices
  #@return a cursor of dictionaries with the data of all clients
  #@raises DatabaseError if something fail while reading the database
  def get_all_devices(self):
    #thread safe
    with self.lock:
      try:
        return self.devices.find({"ip":{"$ne":None}})
      except Exception as e:
        raise DatabaseError("Something went wrong with the database\n" + str(e))

  #get the encryption key of a device by its ip
  #@param ip (str) the ip address of the device
  #@return the encryption key for the device
  #@raises DatabaseError if something fail while reading the database
  def get_device_key_by_ip(self, ip):
    #thread safe
    with self.lock:
      try:
        device = self.devices.find_one({"ip": ip})
        if device is not None:
          return device["key"]
      except Exception as e:
        raise DatabaseError("Something went wrong with the database\n" + str(e))

  #get the encryption key of a device by its name
  #@param name (str) the name with which the device was registered
  #@return the encryption key for the device
  #@raises DatabaseError if something fail while reading the database
  def get_device_key_by_name(self, name):
    #thread safe
    with self.lock:
      try:
        return self.devices.find_one({"name": name})["key"]
      except Exception as e:
        raise DatabaseError("Something went wrong with the database\n" + str(e))

  #delete a device by its ip
  #@param ip (str) the ip address of the device
  #@return True if the deletion process worked, False if no device is associated with that ip
  #@raises DatabaseError if something fail while deleting from the database
  def delete_device_by_ip(self, ip):
    #thread safe
    with self.lock:
      try:
        if self.devices.delete_one({"ip": ip}).deleted_count == 1:
          return True
        else:
          return False
      except Exception as e:
        raise DatabaseError("Something went wrong with the database\n" + str(e))

  #delete a device by its name
  #@param name (str) the name with which the device was registered
  #@return True if the deletion process worked, False if no device is associated with that name
  #@raises DatabaseError if something fail while deleting from the database
  def delete_device_by_name(self, name):
    #thread safe
    with self.lock:
      try:
        if self.devices.delete_one({"name": name}).deleted_count == 1:
          return True
        else:
          return False
      except Exception as e:
        raise DatabaseError("Something went wrong with the database\n" + str(e))

  #register a new suspect in the database
  #@param label (int) an unique index to identify the suspect
  #@param message (str) a message to associate with the suspect,
  # this message will be sent to all clients when the suspect is identified
  #@param photos (list[str]) a list of paths of all photos of the suspect
  #@return True if the insertion process worked
  #@raises DatabaseError if the label was already used
  #@raises DatabaseError if something fail while inserting into database
  def register_suspect(self, label, message, photos):
    #thread safe
    with self.lock:
      try:
        result = self.suspects.insert_one({
                                           "label"   : label,
                                           "message" : message,
                                           "date"    : datetime.datetime.now(),
                                           "photos"  : photos
                                          })
        return True
      except pm.errors.DuplicateKeyError:
        raise DatabaseError("The label '" + label + "' was already registered")
      except Exception as e:
        raise DatabaseError("Something went wrong with the database\n" + str(e))

  #get all registered suspects
  #@return a cursor of dictionaries with the data of the suspects
  #@raises DatabaseError if something fail while reading the database
  def get_all_suspects(self):
    #thread safe
    with self.lock:
      try:
        return self.suspects.find({"label":{"$ne":None}})
      except Exception as e:
        raise DatabaseError("Something went wrong with the database\n" + str(e))

  #get the data of a suspect
  #@param label (str) the label that identify the suspect
  #@return a dictionary with the suspect's data
  #@raises DatabaseError if something fail while reading the database
  def get_suspect(self, label):
    #thread safe
    with self.lock:
      try:
        return self.suspects.find_one({"label": label})
      except Exception as e:
        raise DatabaseError("Something went wrong with the database\n" + str(e))

  #delete a suspect
  #@param label (str) the label that identify the suspect
  #@return True if the deletion process worked, False if no suspect is associated with that label
  #@raises DatabaseError if something fail while deleting from the database
  def delete_suspect(self, label):
    #thread safe
    with self.lock:
      try:
        if self.suspects.delete_one({"label": label}).deleted_count == 1:
          return True
        else:
          return False
      except Exception as e:
        raise DatabaseError("Something went wrong with the database\n" + str(e))

  def get_last_label(self):
    #thread safe
    with self.lock:
      try:
        label = self.suspects.find_one({}, projection={"_id": False, "label": True}, sort=[("label",-1)])["label"]
        return label if label is not None else 0
      except Exception as e:
        raise DatabaseError("Something went wrong with the database\n" + str(e))


  #close the connection with the database
  def close(self):
    #thread safe
    with self.lock:
      self.client.close()
