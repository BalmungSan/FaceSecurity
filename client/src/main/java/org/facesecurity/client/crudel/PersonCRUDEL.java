package org.facesecurity.client.crudel;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.facesecurity.client.jsonformat.PersonJSON;
import org.facesecurity.client.models.Person;
import org.facesecurity.client.security.Cipher;
import org.facesecurity.client.settings.OperationSocket;
import org.json.simple.parser.ParseException;

public class PersonCRUDEL {
	
	private static final Logger LOGGER = Logger.getLogger(PersonCRUDEL.class.getName());
	private static PersonCRUDEL instance = null;
	
	private PersonCRUDEL(){
		
	}
	
	public static PersonCRUDEL getInstance(){
		if(instance == null){
			return new PersonCRUDEL();
		}
		return instance;
	}
	
	public void insert(Person person){
		PersonJSON personJSON = new PersonJSON();
		String message = personJSON.insertJSON(person);
		byte[] encryptedMessage = Cipher.getInstance().encrypt(message);
		OperationSocket operationSocket = OperationSocket.getInstance();
		operationSocket.send(encryptedMessage);
		String out = operationSocket.read();
		try {
			personJSON.getInsertResult(out);
		} catch (ParseException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
	}
	
	public List<Person> getAll(){
		List<Person> people = new ArrayList<>();
		PersonJSON personJSON = new PersonJSON();
		String message = personJSON.listJSON();
		byte[] encryptedMessage = Cipher.getInstance().encrypt(message.getBytes());
		OperationSocket operationSocket = OperationSocket.getInstance();
		operationSocket.send(encryptedMessage);
		String out = operationSocket.read();
		try {
			people = personJSON.getListResult(out);
		} catch (ParseException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
		return people;
	}
	
	public void delete(int label){
		PersonJSON personJSON = new PersonJSON();
		String message = personJSON.deleteJSON(label);
		byte[] encryptedMessage = Cipher.getInstance().encrypt(message);
		OperationSocket operationSocket = OperationSocket.getInstance();
		operationSocket.send(encryptedMessage);
		String out = operationSocket.read();
		try {
			personJSON.getDeleteResult(out);
		} catch (ParseException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
	}
}
