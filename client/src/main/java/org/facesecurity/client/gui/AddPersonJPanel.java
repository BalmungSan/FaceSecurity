package org.facesecurity.client.gui;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.facesecurity.client.crudel.PersonCRUDEL;
import org.facesecurity.client.models.Log;
import org.facesecurity.client.models.Person;
import org.facesecurity.client.settings.LogManager;
public class AddPersonJPanel extends JPanel implements ActionListener{
	private JLabel messageJLabel;
	private JLabel photoJLabel;
	private JLabel nameJLabel;
	private JLabel idJLabel;
	private JLabel photoConfirmationJLabel;
	private JTextArea messageJText;
	private JTextField nameJText;
	private JTextField idJText;
	private JButton newUserJButton;
	private JButton photoJButton;
	private byte[] photo;
	private ArrayList<byte[]> photos = new ArrayList<>();
	private JScrollPane scrollpane;
	private static final Logger LOGGER = Logger.getLogger(AddPersonJPanel.class.getName());
	public AddPersonJPanel(){
		setLayout(null);
		int width = 130;
		int height = 30;
		int x = 70;
		int y = 15;
		
		nameJLabel = new JLabel("Name");
		nameJLabel.setBounds(x, y, width, height);
		
		nameJText = new JTextField("");
		nameJText.setBounds(x*4, y, width, height);
		
		idJLabel = new JLabel("ID");
		idJLabel.setBounds(x, y*4, width, height);
		
		idJText = new JTextField("");
		idJText.setBounds(x*4, y*4, width, height);
		
		messageJLabel = new JLabel("Message");
		messageJLabel.setBounds(x, y*8, width, height);
		
		messageJText = new JTextArea("");
		messageJText.setBounds(x*4, y*8, width*2, height*3);
		messageJText.setLineWrap(true);
		
		scrollpane = new JScrollPane(messageJText);
		scrollpane.setBounds(x*4, y*8, width*2, height*3);
		scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        add(scrollpane, BorderLayout.CENTER);
		
		photoJLabel = new JLabel("Browse photo");
		photoJLabel.setBounds(x,y*15,width,height);
		
		photoJButton = new JButton("Browse");
		photoJButton.setBounds(x*4, y*15, width, height);
		photoJButton.setActionCommand("Upload");
		photoJButton.addActionListener(this);
		
		newUserJButton = new JButton("Add person");
		newUserJButton.setBounds(x*3, y*20, width, height);
		newUserJButton.setActionCommand("AddUser");
		newUserJButton.addActionListener(this);
		
		photoConfirmationJLabel = new JLabel("");
		photoConfirmationJLabel.setBounds(x*4,y*17, 150, height);
		
		add(nameJLabel);
		add(nameJText);
		add(idJLabel);
		add(idJText);
		add(messageJLabel);
		add(photoJLabel);
		add(photoJButton);
		add(newUserJButton);
		add(photoConfirmationJLabel);
	}

	public void upload(){
		photos.clear();
		JFileChooser chooser = new JFileChooser();
		chooser.setMultiSelectionEnabled(true);
		chooser.showOpenDialog(null);
		File[] files  = chooser.getSelectedFiles();
		for(int i = 0; i < files.length; i++){
			String route = files[i].getAbsolutePath();
			try {
				Path path = Paths.get(route);
				photo = Files.readAllBytes(path);
				photos.add(photo);
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, e.toString(), e);
			}
		}
		photoConfirmationJLabel.setText("photo(s) uploaded");
	}
	
	public void addPerson(){
		String message = messageJText.getText();
		String id = idJText.getText();
		String name = nameJText.getText();
		Date date = new Date();

		Person person = new Person(message, photo, date.toString(), photos);
		
		PersonCRUDEL personCRUDEL = PersonCRUDEL.getInstance();
		personCRUDEL.insert(person);
		
		
		ListPersonJPanel listperson = ListPersonJPanel.getInstance();
		listperson.listPeople();
	}
	
	public static void showMessage(String message){
		JOptionPane.showMessageDialog(null, message);
		log(message, new Date());
	}
	
	public static void log(String content, Date date){
		Log log = new Log(date.toString(), content);
		LogManager logManager = new LogManager();
		logManager.setData(log);
	}
	
	public void clean(){
		messageJText.setText("");
		nameJText.setText("");
		idJText.setText("");
		photos.clear();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
			case "Upload":
				upload();
				break;

			case "AddUser":
				addPerson();
				clean();
				break;
			default:
				break;
		}
	}
	
}
