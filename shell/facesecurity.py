#!/usr/bin/env python3.6
# -*- coding: utf-8

'''
    File name: facesecurity.py
    Author: Tomás Felipe Llano Ríos
    Date created: 18/03/2017
    Date last modified: 22/04/2017
    Python Version: 3.6.0
'''

import socket
import pickle
import json
from texttable import Texttable
import sys
sys.path.append('../security')
from cipher import Cipher

SERVER_ADDR = "/tmp/facesecurity.sock"
SHELL_STATUS_RUN = 0
SHELL_STATUS_STOP = 1

help_table = None
uds_socket = None

def send_command_data(data, sock):
  try:
    message = json.dumps(data)
    sock.sendall(uds_cipher.encrypt(message) + b'\n')
    response = pickle.loads(sock.recv(4096))
    return response
  except socket.error as msg:
    sys.exit(msg)

def execute(cmd_tokens, sock):
    command = cmd_tokens[0]
    end = False
    data = None

    if command == "insert":
      data = {"command":"insert", "device":cmd_tokens[1], "ip":cmd_tokens[2],\
              "name":cmd_tokens[3]}
    elif command == "list":
      data = {"command":"list"}
    elif command == "delete":
      param = cmd_tokens[1]
      if param == "name":
        data = {"command":"delete", "name":cmd_tokens[2]}
      elif param == "ip":
        data = {"command":"delete", "ip":cmd_tokens[2]}
      else:
        print("delete does not comprise the parameter:", param)
    elif command == "stop":
      data = {"command":"stop"}
      end = True
    elif command == "exit":
      end = True
    elif command == "help":
      print(help_table.draw())
    else:
      print("Command ", command, " is not valid.")

    if data is not None:
      table = Texttable()
      response = send_command_data(data, sock)
      table.add_rows(response)
      print(table.draw())

    if end:
      sock.close()
      return SHELL_STATUS_STOP
    else:
      return SHELL_STATUS_RUN

def tokenize(line):
    return line.split()

def shell_loop(sock):
    status = SHELL_STATUS_RUN

    while status == SHELL_STATUS_RUN:
        # Command prompt
        sys.stdout.write('> ')
        sys.stdout.flush()
        # Read input
        cmd = sys.stdin.readline()
        # Get command and its arguments"
        cmd_tokens = tokenize(cmd)
        status = execute(cmd_tokens, sock)

def get_help_table_info():
  header = ["COMMAND", "ARGUMENTS", "DESCRIPTION"]

  insert_info = ["insert", " <camera|client>\n <ip>\n <name>",\
                "Inserts camera or client ip into the database."]

  list_info = ["list", "None",\
               "Lists all registered devices (cameras and clients)."]

  delete_info = ["delete", "<name|ip>\n<<name>|<ip>>",\
                 "Deletes device (camera or client) from the database."]

  stop_info = ["stop", "None", "Stop the server execution."]

  exit_info = ["exit", "None", "Exits the command prompt."]

  return [header, insert_info, list_info, delete_info, stop_info, exit_info]


def main():
  global help_table, uds_cipher

  # Create Unix Domain Socket
  s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

  # Initialize global variables
  uds_cipher = Cipher('1234567890ABCDEF')
  help_table = Texttable()
  table_info = get_help_table_info()
  help_table.add_rows(table_info)

  # Connect socket
  try:
    s.connect(SERVER_ADDR)
  except socket.error as msg:
    print(msg)
    sys.exit(1)

  shell_loop(s)
  sys.exit(0)

if __name__ == "__main__":
  main()
