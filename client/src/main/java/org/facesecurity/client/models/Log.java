package org.facesecurity.client.models;

public class Log {
	private String date;
	private String content;
	
	public Log(String date, String content){
		this.date = date;
		this.content = content;
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String format(){
		return date + ": " + content + "\n";
	}
}
