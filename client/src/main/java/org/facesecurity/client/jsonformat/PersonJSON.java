package org.facesecurity.client.jsonformat;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.facesecurity.client.gui.AddPersonJPanel;
import org.facesecurity.client.gui.ListPersonJPanel;
import org.facesecurity.client.models.Person;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class PersonJSON {
	private JSONObject obj;
	private JSONParser parser;
	private static final String COMMAND = "command";
	public PersonJSON(){
		obj = new JSONObject();
		parser = new JSONParser();
	}
	
	public String insertJSON(Person person){
		if(person != null){
			obj.put(COMMAND, "insert");
			obj.put("message", person.getMessage());
			JSONArray photos = new JSONArray();
			for(byte[] photo : person.getPhotos()){
				photos.add(new String(Base64.getEncoder().encode(photo), StandardCharsets.UTF_8));
			}
			obj.put("photos", photos);
		}
		return obj.toJSONString();
	}
	
	public String listJSON(){
		obj.put(COMMAND, "list");
		return obj.toJSONString();
	}
	
	public String deleteJSON(int label){
		obj.put(COMMAND, "delete");
		obj.put("label", label);
		return obj.toJSONString();
	}
	
	public List<Person> getListResult(String resultJSON) throws ParseException{
		List<Person> people = new ArrayList<>();
		JSONObject jsonObject = (JSONObject) parser.parse(resultJSON);
		boolean success = (boolean) jsonObject.get("success");
		if(success){
			JSONArray users = (JSONArray) jsonObject.get("suspects");
			for(int i = 0; i < users.size() ; i++){
				JSONObject user = (JSONObject) users.get(i);
				int label = (int)((long) user.get("label"));
				String date = (String) user.get("date");
				String message = (String) user.get("message");
				String photoString = user.get("photo").toString();
				byte[] photo = Base64.getDecoder().decode(photoString.getBytes());
				people.add(new Person(message, photo, date, label));
			}
		}
		return people;
	}
	
	public void getInsertResult(String resultJSON) throws ParseException{
		JSONObject jsonObject = (JSONObject) parser.parse(resultJSON);
		boolean success = (boolean) jsonObject.get("success");
		String message;
		if(!success){
			 message = (String) jsonObject.get("error");
		}else{
			message = "Person was added successfully";
		}
		AddPersonJPanel.showMessage(message);
	}
	
	public void getDeleteResult(String resultJSON) throws ParseException{
		JSONObject jsonObject = (JSONObject) parser.parse(resultJSON);
		boolean success = (boolean) jsonObject.get("success");
		String message;
		if(!success){
			 message = (String) jsonObject.get("error");
		}else{
			message = "Person was deleted successfully";
		}
		ListPersonJPanel.showMessage(message);
	}
	
}
