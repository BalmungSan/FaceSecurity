package org.facesecurity.client.models;

public class Notification {
	private String message;
	private int confidence;
	private String date;
	private byte[] photoDB;
	private byte[] photoCamera;
	public Notification(String message, int confidence, String date, byte[] photoDB, byte[] photoCamera) {
		this.message = message;
		this.confidence = confidence;
		this.date = date;
		this.photoDB = photoDB;
		this.photoCamera = photoCamera;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getConfidence() {
		return confidence;
	}
	public void setConfidence(int confidence) {
		this.confidence = confidence;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public byte[] getPhotoDB() {
		return photoDB;
	}
	public void setPhotoDB(byte[] photoDB) {
		this.photoDB = photoDB;
	}
	public byte[] getPhotoCamera() {
		return photoCamera;
	}
	public void setPhotoCamera(byte[] photoCamera) {
		this.photoCamera = photoCamera;
	}
}
