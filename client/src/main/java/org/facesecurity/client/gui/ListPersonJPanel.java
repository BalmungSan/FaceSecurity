package org.facesecurity.client.gui;
import java.awt.BorderLayout;
import java.util.Date;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.facesecurity.client.crudel.PersonCRUDEL;
import org.facesecurity.client.models.Log;
import org.facesecurity.client.models.Person;
import org.facesecurity.client.settings.LogManager;

public class ListPersonJPanel extends JPanel{
		
	private static ListPersonJPanel instance = null;
	private JPanel auxJPanel;
	private ListPersonJPanel(){
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		listPeople();
	}
	
	public static ListPersonJPanel getInstance(){
		if(instance == null){
			instance = new ListPersonJPanel();
		}
		return instance;
	}
	
	public static void showMessage(String message){
		JOptionPane.showMessageDialog(null, message);
		log(message, new Date());
	}
	
	public static void log(String content, Date date){
		Log log = new Log(date.toString(), content);
		LogManager logManager = new LogManager();
		logManager.setData(log);
	}

	public void listPeople(){
		this.removeAll();
		PersonCRUDEL personCRUDEL = PersonCRUDEL.getInstance();
		List<Person> people = personCRUDEL.getAll();
		ItemJPanel itemJPanel;
		for( Person person : people){
			itemJPanel = new ItemJPanel(this, person);
			add(itemJPanel);
		}
	}
}