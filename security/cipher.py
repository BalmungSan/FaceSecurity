# -*- coding: utf-8

'''
    File name: cipher.py
    Author: Tomás Felipe Llano Ríos
    Date created: 18/03/2017
    Date last modified: 25/03/2017
    Python Version: 3.6.0
'''

import base64
from Crypto.Cipher import AES
from Crypto import Random

class Cipher:
  def __init__(self, key):
    self.rand = Random.new()
    self.key = key

  def get_random_key(self):
    return base64.b64encode(self.rand.read(16)).decode("UTF-8")

  def encrypt(self, message):
    iv = self.rand.read(AES.block_size)
    cipher = AES.new(self.key, AES.MODE_CFB, iv)
    return base64.b64encode(iv + cipher.encrypt(message))

  def decrypt(self, enc):
    data = base64.b64decode(enc)
    iv = data[:AES.block_size]
    cipher = AES.new(self.key, AES.MODE_CFB, iv)
    return cipher.decrypt(data[AES.block_size:])
