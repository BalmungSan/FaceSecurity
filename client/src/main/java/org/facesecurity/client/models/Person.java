package org.facesecurity.client.models;

import java.util.List;

public class Person {
	String message;
	String name;
	String id;
	List<byte[]> photos;
	byte[] photo;
	String date;
	int label;

	public Person(String message, byte[] photo, String date, int label){
		this.message = message;
		this.photo = photo;
		this.label = label;
		this.date = date;
	}
	
	public Person(String message, byte[] photo, String date, List<byte[]> photos){
		this.message = message;
		this.photo = photo;
		this.date = date;
		this.photos = photos;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public byte[] getPhoto() {
		return photo;
	}
	
	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}
	
	public List<byte[]> getPhotos() {
		return this.photos;
	}
	
	public void addPhoto(byte[] photo) {
		this.photos.add(photo);
	}
	
	public int getLabel() {
		return label;
	}

	public void setLabel(int label) {
		this.label = label;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
