package org.facesecurity.client.gui;

import java.awt.Dimension;
import java.awt.Image;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.facesecurity.client.models.Log;
import org.facesecurity.client.models.Notification;
import org.facesecurity.client.settings.LogManager;

public class MessageJOptionPane {
	
	private static final Logger LOGGER = Logger.getLogger(AddPersonJPanel.class.getName());
	
	private Notification notification = null;
	public MessageJOptionPane(Notification notification){
		this.notification = notification;
		JOptionPane.showConfirmDialog(null, getPanel(), "Warning", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
	}
	
	public JPanel getPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setPreferredSize(new Dimension(500, 230));
		String confidenceLevel = getConfidenceLevel(panel);
		String baseMessage = "<html> " +
												   "It has been identified a person registered on " + this.notification.getDate() +
												   "<br/> with a " + confidenceLevel +" level of confidence"+ 
												 "</html>";
		log(confidenceLevel);
		
		JLabel baseMessageJLabel = new JLabel(baseMessage);
		baseMessageJLabel.setBounds(50, 15, 500, 30);
		
		JLabel messageJLabel = new JLabel("Message: " + notification.getMessage());
		messageJLabel.setBounds(50, 15*3, 500, 30);
		
		ImageIcon photoDBIcon = new ImageIcon(notification.getPhotoDB());
		Image photoDBImage = photoDBIcon.getImage();
		Image photoDBImg = photoDBImage.getScaledInstance(80, 120,  java.awt.Image.SCALE_SMOOTH);
		photoDBIcon = new ImageIcon(photoDBImg);
		
		ImageIcon photoCameraIcon = new ImageIcon(notification.getPhotoCamera());
		Image photoCameraImage = photoCameraIcon.getImage();
		Image photoCameraImg = photoCameraImage.getScaledInstance(80, 120,  java.awt.Image.SCALE_SMOOTH);
		photoCameraIcon = new ImageIcon(photoCameraImg);
		
    JLabel photoDBLabel = new JLabel(photoDBIcon);
    photoDBLabel.setBounds(150, 15*6, 80, 120);
    JLabel photoCameraLabel = new JLabel(photoCameraIcon);
    photoCameraLabel.setBounds(250, 15*6, 80, 120);
    
    panel.add(baseMessageJLabel);
    panel.add(messageJLabel);
    panel.add(photoDBLabel);
    panel.add(photoCameraLabel);
    return panel;
	}
	
	public String getConfidenceLevel(JPanel panel){
		int confidence = this.notification.getConfidence();
		ImageIcon confidenceIcon;
		String result;
		switch (confidence) {
			case 1:
				confidenceIcon = new ImageIcon(this.getClass().getResource("/high.png"));
				result = "high";
				break;
			case 0:
				confidenceIcon = new ImageIcon(this.getClass().getResource("/medium.png"));
				result = "medium";
				break;
			default:
				confidenceIcon = new ImageIcon(this.getClass().getResource("/low.png"));
				result = "low";
				break;
		}
		try{
			Image confidenceImage = confidenceIcon.getImage();
			confidenceImage = confidenceImage.getScaledInstance(32, 32,  java.awt.Image.SCALE_SMOOTH);
			confidenceIcon = new ImageIcon(confidenceImage);
			JLabel label = new JLabel("", confidenceIcon, JLabel.CENTER);
			label.setBounds(0, 5, 32, 32);
			panel.add(label);
			return result;
		}catch(NullPointerException e){
			LOGGER.log(Level.SEVERE, e.toString(), e);
			return "";
		}
		
	}
	
	public void log(String confidenceLevel){
		String content = "It has been identified a person with a " + confidenceLevel +" level of confidence";
		Log log = new Log(this.notification.getDate(), content);
		LogManager logManager = new LogManager();
		logManager.setData(log);
	}
}
