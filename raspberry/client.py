#!/usr/bin/env python3.6
import sys
sys.path.append("/opt/python3/lib/python3.6/site-packages")
sys.path.append("../security/")
import socket
import cv2
import os
import numpy as np
from cipher import Cipher

face_cascade = None
FRAME_RATE = 30
video_capture = None
s = None
cpr = None
CASC_PATH = "../server/facerecognizer/haarcascade_frontalface_default.xml"

def detect_face():
  # Counter for each frame of the usb cam
  frame_count = 0
  while True:
    # Frame taken of the camera
    ret, frame = video_capture.read()
    frame_count += 1
    # Process one frame each 15 frames or 0.5 sec
    if frame_count == FRAME_RATE:
      # Restart the counter after 15 frames or 0.5 sec
      frame_count = 0
      # Convert to Grayscale the frame
      gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
      # Convert to numpy array the image to perform some operations over it
      image = np.array(gray, 'uint8')
      # Detect faces in the image
      faces = face_cascade.detectMultiScale(
         image,
         scaleFactor=1.2,
         minNeighbors=5,
         minSize=(30, 30)
      )
      # Array to save only the faces of the image
      if len(faces) > 0:
        for x, y, w, h in faces:
          recognized_faces = image[y:y+h, x:x+w]

        img_bytes = recognized_faces.dumps()
        s.sendall(cpr.encrypt(img_bytes) + b'\n')
        print("Done Sending")

def main(argv):
  global face_cascade, video_capture, s, cpr
  face_cascade = cv2.CascadeClassifier(CASC_PATH)
  video_capture = cv2.VideoCapture(0)
  # Set Frame Width 4 = CV_CAP_PROP_FRAME_WIDTH
  video_capture.set(4,1920)
  # Set Frame Heigth 5 = CV_CAP_PROP_FRAME_HEIGHT
  video_capture.set(5,1080)

  # Create a socket object
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  # Allow to reuse the ip address
  s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
  # Reserve a port for your service
  port = int(os.environ.get("HOST_PORT"))
  host = os.environ.get("HOST_IP")
  s.connect((host, port))

  key = os.environ.get("KEY")
  cpr = Cipher(key)

  detect_face()

if __name__ == "__main__":
  main(sys.argv)
