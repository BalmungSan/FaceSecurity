package org.facesecurity.client.settings;

import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.facesecurity.client.gui.ItemJPanel;
import org.facesecurity.client.gui.MessageJOptionPane;
import org.facesecurity.client.jsonformat.NotificationJSON;
import org.facesecurity.client.security.Cipher;

public class NotificationSocket implements Runnable{
	
	private Socket socket;
	private static final Logger LOGGER = Logger.getLogger(ItemJPanel.class.getName());

	public NotificationSocket(){
		SettingsManager settings = SettingsManager.getInstance();
		if(settings.isCheck()){
			String ip = settings.getIp();
			int notificationPort = Integer.parseInt(settings.getNotificationPort());
			try {
				this.socket = new Socket();
				this.socket.connect(new InetSocketAddress(InetAddress.getByName(ip), notificationPort));
			}catch (SocketTimeoutException ste) {
				LOGGER.log( Level.SEVERE, ste.toString(), ste);
			}catch (UnknownHostException uhe) {
				LOGGER.log( Level.SEVERE, uhe.toString(), uhe);
			}catch (IOException e) {
				LOGGER.log( Level.SEVERE, e.toString(), e);
			} 
		}
	}	

	public void close() throws IOException {
		socket.close();
	}
	
	@Override
	public void run() {
		LOGGER.info("Trying to read...");
		try (BufferedReader br = new BufferedReader(new InputStreamReader(this.socket.getInputStream()))) {
				String data;
				String jsonMessage;
				while((data = br.readLine()) != null){
					jsonMessage = new String(Cipher.getInstance().decrypt(data.getBytes()));
					NotificationJSON notificationJSON = new NotificationJSON();
					new MessageJOptionPane(notificationJSON.getInformation(jsonMessage));
				}
		} catch (IOException|ParseException e) {
			LOGGER.log( Level.SEVERE, e.toString(), e);
		}
	}
}