import unittest
import sys
sys.path.append("security")
from cipher import Cipher

#TestCipher
#this class contains unit tests for the cipher class
class TestCipher(unittest.TestCase):
  #this test ensure that for random keys
  #the cipher is capable of encrypt different messages
  #and then decrypt them into the original messages
  def tests(self):
    words = ["facescurity", "Hello World!", "he's saying` \"hello\"", "",
             "@#$%&/()=?¿¡[]{}*+~<>-_:;.,", "âÂèÈiíÍüÜóÓñÑ", "0123456789", "s3cr3t"]
    random_generator = Cipher("")
    tests = 10
    while tests > 0:
      key = random_generator.get_random_key()
      cipher = Cipher(key)
      for word in words:
        encrypted = cipher.encrypt(word)
        decrypted = cipher.decrypt(encrypted).decode()
        self.assertEqual(word, decrypted)
      tests -= 1
