package org.facesecurity.client.security;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * CipherTests
 * this class contains unit tests for the cipher class
 */
public class CipherTest {
  /**
   * this test ensure that the cipher is capable of
   * encrypt different messages and then decrypt them
   * into the original messages
   */
  @Test
  public void tests() {
    Cipher cipher = Cipher.getTestInstance("0123456789ABCDEF");
    String[] words = {"facescurity", "Hello World!", "he's saying` \"hello\"", "",
                      "@#$%&/()=?¿¡[]{}*+~<>-_:;.,", "âÂèÈiíÍüÜóÓñÑ", "0123456789", "s3cr3t"};
    for (String word : words) {
      byte[] encrypted = cipher.encrypt(word);
      byte[] decrypted = cipher.decrypt(encrypted);
      String word_decr = new String(decrypted);
      assertEquals(word, word_decr);
    }
  }
}
