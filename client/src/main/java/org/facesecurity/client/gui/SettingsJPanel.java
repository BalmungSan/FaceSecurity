package org.facesecurity.client.gui;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.facesecurity.client.models.Log;
import org.facesecurity.client.settings.LogManager;
import org.facesecurity.client.settings.SettingsManager;
public class SettingsJPanel extends JPanel implements ActionListener, KeyListener{
	JLabel ipJLabel;
	JLabel notificationPortJLabel;
	JLabel uploadKeyJLabel;
	JLabel keyJLabel;
	JLabel operationPortJLabel;
	JTextField ipJText;
	JTextField notificationPortJText;
	JTextField operationPortJText;
	JButton uploadJButton;
	JButton setupJButton;
	String key = "";
	SettingsManager settings;
	private static final Logger LOGGER = Logger.getLogger(ItemJPanel.class.getName());
	
	public SettingsJPanel(){
		setLayout(null);
		int width = 130;
		int height = 30;
		int x = 70;
		int y = 15;
		
		settings = SettingsManager.getInstance();
		key = settings.getKey();
		
		ipJLabel = new JLabel("IP");
		ipJLabel.setBounds(x, y, width, height);
		
		ipJText = new JTextField(settings.getIp());
		ipJText.setBounds(x*6, y, width, height);
		
		notificationPortJLabel = new JLabel("NOTIFICATIONS' PORT");
		notificationPortJLabel.setBounds(x, y*4, width + 20, height);
		
		notificationPortJText = new JTextField(settings.getNotificationPort());
		notificationPortJText.setBounds(x*6, y*4, width, height);
		
		operationPortJLabel = new JLabel("OPERATIONS' PORT");
		operationPortJLabel.setBounds(x, y*8, width + 20, height);
		
		operationPortJText = new JTextField(settings.getOperationPort());
		operationPortJText.setBounds(x*6, y*8, width, height);
		
		uploadKeyJLabel = new JLabel("KEY");
		uploadKeyJLabel.setBounds(x, y*11, width, height);
		
		uploadJButton = new JButton("Browse key");
		uploadJButton.setBounds(x*6, y*11, width, height);
		uploadJButton.setActionCommand("Upload");
		uploadJButton.addActionListener(this);
		
		keyJLabel = new JLabel(settings.getKey());
		keyJLabel.setBounds(x*6, y*13, width + 20, height);
		
		setupJButton = new JButton("OK");
		setupJButton.setBounds(x*3, y*16, width, height);
		setupJButton.setActionCommand("Setup");
		setupJButton.addActionListener(this);
		
		notificationPortJText.addKeyListener(this);
		operationPortJText.addKeyListener(this);
		
		add(ipJLabel);
		add(ipJText);
		add(notificationPortJLabel);
		add(notificationPortJText);
		add(operationPortJLabel);
		add(operationPortJText);
		add(uploadKeyJLabel);
		add(uploadJButton);
		add(setupJButton);
		add(keyJLabel);
	}
	
	public String upload(){
		String aux = "";
		StringBuilder keyReturnBuild = new StringBuilder();
		JFileChooser chooser = new JFileChooser();
		BufferedReader buf;
        int returnVal = chooser.showOpenDialog(null);
        File chosenFile;
        if((returnVal == JFileChooser.APPROVE_OPTION) && ((chosenFile = chooser.getSelectedFile()) != null)) {
        	try(FileReader files = new FileReader(chosenFile)){
        		buf = new BufferedReader(files);
        		while((aux = buf.readLine()) != null){
        			keyReturnBuild.append(aux);
            	}
            	buf.close();
        	}catch(IOException e){
        		LOGGER.log( Level.SEVERE, e.toString(), e);
        	}
        }
        String keyReturn = keyReturnBuild.toString();
        keyJLabel.setText(keyReturn);
    	return keyReturn;
	}
	
	public void setup(){
		String ip = ipJText.getText();
		String notificationPort = notificationPortJText.getText();
		String operationPort = operationPortJText.getText();
		settings.setAll(ip, notificationPort, operationPort, key);
		String content = "Settings updated";
		Date date = new Date();
		Log log = new Log(date.toString(), content);
		LogManager logManager = new LogManager();
		logManager.setData(log);
    	JOptionPane.showMessageDialog(null, content);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Upload":
			key = upload();
			break;

		case "Setup":
			setup();
			break;
		default:
			break;
		}
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		 int keyNum = e.getKeyCode();

         /* Restrict input to only integers */
         if (keyNum < 96 && keyNum > 105){
        	 e.consume();
         }
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
		if (!Character.isDigit(e.getKeyChar())){
			e.consume();
		}
	}
}
