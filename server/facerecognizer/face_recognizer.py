#!/usr/bin/env python3.6

import sys
sys.path.append('/usr/local/lib/python3.6/site-packages')
import cv2, os
import numpy as np

# File that allows to detect a face in an image
cascade_path = "haarcascade_frontalface_default.xml"
# Object to detect a face in an image
face_cascade = cv2.CascadeClassifier(cascade_path)
# Facial Recognizer based on Local Binary Patterns Histograms
recognizer = cv2.face.createLBPHFaceRecognizer()

def get_images(path):
  # join all image paths in a list
  images_path = [os.path.join(path, f) for f in os.listdir(path)]
  # Array that will contain face images
  images = []
  # Array that will contain tha label that is assigned to the image'))
  labels = []
  for image_path in images_path:
     # Read each image
     original_image = cv2.imread(image_path)
     # Convert each image to grayscale
     gray_image = cv2.cvtColor(original_image, cv2.COLOR_BGR2GRAY)
     # Convert the image format into numpy array
     image = np.array(gray_image, 'uint8')
     # Get label of the image
     label = int(os.path.split(image_path) [1].split("_") [0].replace("sujeto", ""))
     # Detect the face in the image
     face = face_cascade.detectMultiScale(
        image,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30))
     # If face is detected, append the face to images array  and the label to labels array
     for x, y, w, h in face:
        images.append(image[y:y+h, x:x+w])
        labels.append(label)

  return images, labels

def recognize_person(suspect):

  label_predicted, conf = recognizer.predict(suspect)

  if conf >= 0 and conf <= 10:
    return True, label_predicted, 1
  elif conf > 10 and conf <= 30:
    return True, label_predicted, 0
  elif conf > 30 and conf <= 40:
    return True, label_predicted, -1
  else:
    return False, None, None
