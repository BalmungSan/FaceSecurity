package org.facesecurity.client.settings;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.facesecurity.client.gui.ItemJPanel;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class SettingsManager {
	
	private static SettingsManager instance = null;
	
	String name = "settings.key";
	String route = "";
	String ip = "";
	String notificationPort = "";
	String key = "";
	String operationPort = "";
	boolean check = false;

	private static final Logger LOGGER = Logger.getLogger(ItemJPanel.class.getName());
	
	private SettingsManager(){
		route = getRoute() + name;
		try{
			File file = new File(route);
			if(file.exists()){
				JSONParser parser = new JSONParser();
				Object obj = parser.parse(new FileReader(file));
				JSONObject jsonObject = (JSONObject) obj;
				ip = (String) jsonObject.get("ip");
				notificationPort = (String) jsonObject.get("notificationPort");
				operationPort = (String) jsonObject.get("operationPort");
				key = (String) jsonObject.get("key");
				check = true;
			}
		}catch(IOException|ParseException e){
			LOGGER.log( Level.SEVERE, e.toString(), e);
		}
	}
	
	public static SettingsManager getInstance(){
		if(instance == null){
			instance = new SettingsManager();
		}
		return instance;
	}
	
	private String getRoute(){
		String fileRoute = new File(".").getAbsolutePath();
		return fileRoute.substring(0, fileRoute.length() - 1);
	}
	
	private void write(){
		JSONObject obj = new JSONObject();
		obj.put("ip", ip);
		obj.put("notificationPort", notificationPort);
		obj.put("operationPort", operationPort);
		obj.put("key", key);
		try (FileWriter file = new FileWriter(route)) {
			file.write(obj.toJSONString());
		}catch(IOException e){
			LOGGER.log( Level.SEVERE, e.toString(), e);
		}
	}
	
	public void setAll(String ip, String notificationPort, String operationPort, String key){
		this.ip = ip;
		this.notificationPort = notificationPort;
		this.operationPort = operationPort;
		this.key = key;
		write();
	}
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getNotificationPort() {
		return notificationPort;
	}

	public void setNotificationPort(String notificationPort) {
		this.notificationPort = notificationPort;
	}

	public String getOperationPort() {
		return operationPort;
	}

	public void setOperationPort(String operationPort) {
		this.operationPort = operationPort;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	public boolean isCheck() {
		return check;
	}

	public void setCheck(boolean check) {
		this.check = check;
	}

}
