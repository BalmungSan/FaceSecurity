#!/usr/bin/env python3.6
# -*- coding: utf-8

'''
    File name: server.py
    Author: Tomás Felipe Llano Ríos
    Date created: 18/03/2017
    Date last modified: 22/04/2017
    Python Version: 3.6.0
'''

import sys
sys.path.append('../security/')
sys.path.append('../database/')
import cv2
import base64
import os
import time
import threading
import pickle
import json
import numpy as np
import socketserver as ss
from cipher import Cipher
from select import select
from database_manager  import DatabaseManager, DatabaseError
from texttable import Texttable
from facerecognizer.face_recognizer import recognizer,\
    recognize_person, face_cascade

#
db         = None
message    = None
notify_cv  = None
run_cv     = None
run        = None
count      = None
threads    = None
uds_cipher = None
label      = None
servers    = None
recognizer_lock = None

#
def add_suspect(photos):
  global label
  label  += 1
  images  = []
  labels  = [label]
  paths   = []
  counter = 0
  for photo in photos:
    np_arr = np.frombuffer(photo, np.uint8)
    img_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
    paths.append("/home/user1/photos/sujeto{}_{}.jpg".format(label, counter))
    cv2.imwrite(paths[counter], img_np)
    gray_img = cv2.cvtColor(img_np, cv2.COLOR_BGR2GRAY)
    face = face_cascade.detectMultiScale(
        gray_img,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30))

    for x, y, w, h in face:
      images.append(gray_img[y:y+h, x:x+w])

    counter +=1
  labels = np.array(labels*len(images))
  recognizer.update(images, labels)
  print("Update Done")

  return label, paths

#
class CameraRequestHandler(ss.StreamRequestHandler):
  def to_jpeg(self, photo):
    encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 90]
    result, img = cv2.imencode(".jpg", photo, encode_param)
    return img.tobytes()

  def set_message(self, label, confidence, photo):
    global message, threads, count
    with notify_cv:
      notify_cv.wait_for(lambda: count == 0)
      suspect_info = db.get_suspect(label)

      #
      data = {}
      data["message"] = suspect_info["message"]
      data["confidence"] = confidence
      data["date"] = suspect_info["date"].strftime("%Y-%m-%d %H:%M:%S")
      with open(suspect_info["photos"][0], "rb") as image_file:
        photo_db = image_file.read()
      data["photo_db"] = base64.b64encode(photo_db).decode("utf-8")
      photo = self.to_jpeg(photo)
      data["photo_camera"] = base64.b64encode(photo).decode("utf-8")
      message = json.dumps(data)

      #
      for client_ip in threads:
        client_cv = threads[client_ip]["cv"]
        with client_cv:
          threads[client_ip]["ready"] = True
          client_cv.notify()
      count = len(threads)

  def handle(self):
    global threads, count, message, db, run
    print("Connection Accepted from camera {}".format(self.client_address))
    camera_ip = self.client_address[0]
    key = db.get_device_key_by_ip(camera_ip)
    self.cipher = Cipher(key)
    while run:
      readable, _, _ = select([self.request], [], [], 1)
      if readable:
        line = self.rfile.readline()
        #socket was closed
        if line == b'':
          break

        photo = self.cipher.decrypt(line)
        photo = pickle.loads(photo)
        with recognizer_lock:
          recognized, label, confidence = recognize_person(photo)
        if recognized:
          threading.Thread(target = self.set_message,
                           args=(label, confidence, photo)).start()

    print("Connection Terminated with camera {}".format(self.client_address))

#
class ClientRequestHandler(ss.StreamRequestHandler):
  def create_suspect(self, s):
    suspect = {}
    suspect["label"]   = s["label"]
    suspect["message"] = s["message"]
    suspect["date"]    = s["date"].strftime("%Y-%m-%d %H:%M:%S")
    with open(s["photos"][0], "rb") as image_file:
      photo = base64.b64encode(image_file.read()).decode("utf-8")
    suspect["photo"] = photo
    return suspect

  def parse_request(self, data):
    command  = data["command"]
    response = {}
    try:
      if command == "insert":
        photos = list(map(lambda photo: base64.b64decode(photo.encode()), data["photos"]))
        label, photos_paths = add_suspect(photos)
        db.register_suspect(label, data["message"], photos_paths)
        response["success"] = True
      elif command == "list":
        suspects = []
        for s in db.get_all_suspects():
          suspect = self.create_suspect(s)
          suspects.append(suspect)
        response["success"] = True
        response["suspects"] = suspects
      elif command == "delete":
        label = data["label"]
        for path in db.get_suspect(label)["photos"]:
          os.remove(path)
        db.delete_suspect(label)
        if os.path.exists("./recognizer.xml"):
          os.remove("./recognizer.xml")
        threading.Thread(target=init_recognizer).start()
        response["success"] = True
      else:
        response["success"] = False
        response["error"]   = command + ": command not valid"
    except DatabaseError as dbe:
      response["success"] = False
      response["error"]   = str(dbe)
    return response

  def handle(self):
    global db
    print("Connection Accepted from client {}".format(self.client_address))
    client_ip = self.client_address[0]
    key = db.get_device_key_by_ip(client_ip)
    self.cipher = Cipher(key)

    while run:
      readable, _, _ = select([self.request], [], [], 1)
      if readable:
        line = self.rfile.readline()
        #socket was closed
        if line == b'':
          client_ip = self.client_address[0]
          client_cv = threads[client_ip]["cv"]
          with client_cv:
            threads[client_ip]["ready"] = True
            threads[client_ip]["run"] = False
            client_cv.notify()
          break

        data = json.loads(self.cipher.decrypt(line).decode())
        response = self.parse_request(data)

        #send the response to the client
        msg = self.cipher.encrypt(json.dumps(response).encode())
        self.wfile.write(msg + b'\n')

    print("Connection Terminated with client {}".format(self.client_address))

#
class NotifyRequestHandler(ss.StreamRequestHandler):
  def handle(self):
    global threads, count, message, notify_cv, run
    print("Connection Accepted from notify {}".format(self.client_address))
    client_ip = self.client_address[0]
    key = db.get_device_key_by_ip(client_ip)
    self.cipher = Cipher(key)
    self.cv = threading.Condition(threading.Lock())
    threads[client_ip] = {"cv":self.cv, "ready":False, "run":True}

    while threads[client_ip]["run"]:
      with self.cv:
        self.cv.wait_for(lambda: threads[client_ip]["ready"])
        #check if we were awaken to stop execution
        if not threads[client_ip]["run"]:
          continue
        #else, send the notification
        encrypted_message = self.cipher.encrypt(message)
        self.wfile.write(encrypted_message + b'\n')
        threads[client_ip]["ready"] = False

        with notify_cv:
          count -= 1
          if count == 0:
            notify_cv.notify()

    del threads[client_ip]
    print("Connection Terminated with notify {}".format(self.client_address))

#
class UDSRequestHandler(ss.StreamRequestHandler):
  def parse_commands(self, commands):
    global db, uds_cipher, run_cv, run

    command = commands["command"]
    try:
      if command == "insert":
        ip = commands["ip"]
        device = commands["device"]
        if device == "camera":
          key = uds_cipher.get_random_key()
          name = commands["name"]
          db.register_camera(ip, name, key)
          msg = [["KEY"], [key]]
        else:
          key = uds_cipher.get_random_key()
          name = commands["name"]
          db.register_client(ip, name, key)
          msg = [["KEY"], [key]]
        self.wfile.write(pickle.dumps(msg))
      elif command == "list":
        table = Texttable()
        table_info = [["IP", "NOMBRE", "TIPO", "KEY"]]
        for device in db.get_all_devices():
          table_info.append([device["ip"], device["name"], device["type"],\
                             device["key"]])
        msg = pickle.dumps(table_info)
        self.wfile.write(msg)
      elif command == "delete":
        if "name" in commands:
          db.delete_device_by_name(commands["name"])
          msg = [["STATUS"], ["success"]]
        else:
          db.delete_device_by_ip(commands["ip"])
          msg = [["STATUS"], ["success"]]
        self.wfile.write(pickle.dumps(msg))
      elif command == "stop":
        msg = [["STATUS"], ["success"]]
        self.wfile.write(pickle.dumps(msg))
        with run_cv:
          run = False
          run_cv.notify()
      else:
        print("Error: Unknow command", command)
    except DatabaseError as error:
      msg = [["STATUS", "REASON"], ["failed", str(error)]]
      self.wfile.write(pickle.dumps(msg))

  def handle(self):
    global uds_cipher
    print("Connection Accepted from uds")
    while run:
      data = self.rfile.readline()
      if data != b'':
        data = uds_cipher.decrypt(data)
        commands = json.loads(data.decode())
        self.parse_commands(commands)
      else:
        break

    print("Connection Terminated with uds")

#
def init_recognizer():
  global db, recognizer_lock
  with recognizer_lock:
    images = []
    labels = []
    suspects = list(db.get_all_suspects())
    if not suspects:
      print("Not suspects registered yet")
      return None

    for suspect in suspects:
      label = suspect["label"]
      print("Label: {}".format(label))
      for path in suspect["photos"]:
        print("Training: {}".format(path))
        original_image = cv2.imread(path)
        gray_image = cv2.cvtColor(original_image, cv2.COLOR_BGR2GRAY)
        # Convert the image format into numpy array
        image = np.array(gray_image, 'uint8')
        # Detect the face in the image
        face = face_cascade.detectMultiScale(
          image,
          scaleFactor=1.2,
          minNeighbors=5,
          minSize=(100, 100))
        # If face is detected, append the face to images array  and the label
        # to labels array
        for x, y, w, h in face:
          images.append(image[y:y+h, x:x+w])
          labels.append(label)

    # Train the face recognizer
    recognizer.train(images, np.array(labels, dtype="int"))

    # Save the Recognizer
    recognizer.save("./recognizer.xml")

#
def init_server():
  global servers
  # Set connection information
  bind_ip     = os.environ.get("FS_BIND_IP")
  camera_port = int(os.environ.get("FS_CAMERA_PORT"))
  client_port = int(os.environ.get("FS_CLIENT_PORT"))
  notify_port = int(os.environ.get("FS_NOTIFY_PORT"))
  uds_address = "/tmp/facesecurity.sock"

  # Establish connection
  try:
    ss.TCPServer.allow_reuse_address = True
    camera_server = ss.ThreadingTCPServer((bind_ip, camera_port), CameraRequestHandler)
    client_server = ss.ThreadingTCPServer((bind_ip, client_port), ClientRequestHandler)
    notify_server = ss.ThreadingTCPServer((bind_ip, notify_port), NotifyRequestHandler)
    unixds_server = ss.ThreadingUnixStreamServer(uds_address, UDSRequestHandler)
    servers = [camera_server, client_server, notify_server, unixds_server]
  except PermissionError:
    sys.exit("Error creating the server sockets")

  camera_server_thread = threading.Thread(target=camera_server.serve_forever)
  camera_server_thread.daemon = True
  camera_server_thread.start()

  client_server_thread = threading.Thread(target=client_server.serve_forever)
  client_server_thread.daemon = True
  client_server_thread.start()

  notify_server_thread = threading.Thread(target=notify_server.serve_forever)
  notify_server_thread.daemon = True
  notify_server_thread.start()

  unixds_server_thread = threading.Thread(target=unixds_server.serve_forever)
  unixds_server_thread.daemon = True
  unixds_server_thread.start()

#main method
def main():
  global threads, count, notify_cv, run_cv, run, db, uds_cipher, label, servers, recognizer_lock
  #initialize the database connection
  db_ip      = os.environ.get("FS_DB_IP")
  db_port    = os.environ.get("FS_DB_PORT")
  db         = DatabaseManager(db_ip, int(db_port))

  #load recognizer
  recognizer_lock = threading.Lock()
  if os.path.exists("./recognizer.xml"):
    recognizer.load("./recognizer.xml")
  else:
    init_recognizer()
  print("Training Done")

  #initialize global variables
  label      = db.get_last_label()
  uds_cipher = Cipher("1234567890ABCDEF")
  notify_cv  = threading.Condition(threading.Lock())
  run_cv     = threading.Condition(threading.Lock())
  run        = True
  count      = 0
  threads    = {}
  message    = ""

  #start the server
  init_server()
  print("Server ready")

  #wait until run is false
  with run_cv:
    while run:
      run_cv.wait()
    #awake all notify threads
    for client_ip in threads:
      client_cv = threads[client_ip]["cv"]
      with client_cv:
        threads[client_ip]["ready"] = True
        threads[client_ip]["run"] = False
        client_cv.notify()
    #stop the server and release system resources
    for server in servers:
      server.shutdown()
      server.server_close()
    #delet the unix domain socket
    os.remove("/tmp/facesecurity.sock")
  print("server stopped successfully")
  sys.exit(0)

#start the server
if __name__ == "__main__":
  main()
