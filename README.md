#FaceSecurity

##Introducción:
Aplicación de reconocimiento facial para incrementar la seguridad en los espacios abiertos de instituciones privadas,
como universidades, centros comerciales o estaciones de sistemas de transporte público.

##Miembros y Roles:
* Tomás Felipe Llano Ríos     - DevOps (Servidor y Seguridad)
* Luis Miguel Mejía Suárez    - DevOps (Base de Datos y Seguridad)
* Juan Diego Ocampo García    - DevOps (Cliente y Sistemas Embebidos)
* Johan Sesbastián Yepes Ríos - DevOps (Conexión y Sistemas Embebidos)

##Dependencias:
### Servidor:
* Python 3.6 & Pip
   - OpenCV
   - Numpy
   - PyCrypto
   - PyMongo
* MongoDB 3.4

### Cliente:
* Java Runtime Environmet (JRE) 8
* Java Cryptographic Extension (JCE)

### Embebido:
* Python 3.6 & Pip
   - PyCrypto
   - OpenCV
   - Numpy
