package org.facesecurity.client.settings;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.facesecurity.client.gui.ItemJPanel;
import org.facesecurity.client.gui.LogsJPanel;
import org.facesecurity.client.models.Log;

public class LogManager {

	private static final Logger LOGGER = Logger.getLogger(ItemJPanel.class.getName());
	private String name = "log.log";
	private String route = "";
	BufferedReader in = null;
	FileReader fileReader = null;
	public LogManager(){
		route = getRoute() + name;
		try {
			File file = new File(route);
			if (!file.isFile() && !file.createNewFile()){
		        throw new IOException("Error creating new file: " + file.getAbsolutePath());
		    }
			fileReader = new FileReader(file);
			in = new BufferedReader(fileReader);
		} catch (FileNotFoundException fnfe) {
			LOGGER.log( Level.SEVERE, fnfe.toString(), fnfe);
		} catch (IOException e) {
			LOGGER.log( Level.SEVERE, e.toString(), e);
		}
		
	}
	
	public void close(){
		try {
			in.close();
			fileReader.close();
		} catch (IOException e) {
			LOGGER.log( Level.SEVERE, e.toString(), e);
		}
	}
	
	public void setData(Log log){
        try (FileWriter fw = new FileWriter(route,true)){
            fw.write(log.format());//appends the string to the file
        } catch ( IOException e ) {
        	LOGGER.log( Level.SEVERE, e.toString(), e);
        }
    		LogsJPanel logJPanel = LogsJPanel.getInstance();
    		logJPanel.setData();
	}
	
	public String getData(){
		String line;
		StringBuilder strBuilder =  new StringBuilder();
		try {
			while((line = in.readLine()) != null){
				strBuilder.append(line + "\n");
			}
			in.close();
		} catch (IOException e) {
			LOGGER.log( Level.SEVERE, e.toString(), e);
		}
		return new String(strBuilder);
	}
	
	private String getRoute(){
		String fileRoute = new File(".").getAbsolutePath();
		return fileRoute.substring(0, fileRoute.length() - 1);
	}
}
