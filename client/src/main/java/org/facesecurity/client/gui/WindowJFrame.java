package org.facesecurity.client.gui;
import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.*;
import javax.swing.border.Border;

public class WindowJFrame extends JFrame{
	public WindowJFrame(boolean check){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(600,400);
		setTitle("FaceSecurity");

		//Create tabs
		JTabbedPane tabs=new JTabbedPane();

		if(check){
			LogsJPanel logsJPanel = LogsJPanel.getInstance();
			tabs.add("Logs", logsJPanel);
			
			AddPersonJPanel addPersonJPanel = new AddPersonJPanel();
			tabs.addTab("Add person", addPersonJPanel);
			
			ListPersonJPanel listPersonJPanel =  ListPersonJPanel.getInstance();
			tabs.addTab("List people", listPersonJPanel);
		}
		SettingsJPanel settingsPanel = new SettingsJPanel();
		tabs.addTab("Settings", settingsPanel);

		getContentPane().add(tabs);
	    		
		setVisible(true);
		//setResizable(false);
	}
}
