package org.facesecurity.client;

import java.net.SocketException;
import org.facesecurity.client.gui.WindowJFrame;
import org.facesecurity.client.settings.NotificationSocket;
import org.facesecurity.client.settings.SettingsManager;

public class Main{
	
	private Main(){
		// Not called
	}

	public static void main(String[] args) throws SocketException {
		SettingsManager settings = SettingsManager.getInstance();
		if(settings.isCheck()){
			(new Thread(new NotificationSocket())).start();
		}
		new WindowJFrame(settings.isCheck());
	}
}
