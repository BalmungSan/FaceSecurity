package org.facesecurity.client.gui;
import java.awt.BorderLayout;

import javax.swing.*;

import org.facesecurity.client.settings.LogManager;

public class LogsJPanel extends JPanel{
	
	private static JTextArea logJTextArea;
	private static LogsJPanel instance = null;
	private JScrollPane scrollpane;
	private LogsJPanel(){
		setLayout(new BorderLayout());
		logJTextArea = new JTextArea("");
		logJTextArea.setEditable(false);
		setData();
		scrollpane = new JScrollPane(logJTextArea);
		scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        add(scrollpane, BorderLayout.CENTER);
	}
	
	public static LogsJPanel getInstance(){
		if(instance == null){
			instance = new LogsJPanel();
		}
		return instance;
	}
	
	public void setData(){
		
		logJTextArea.setText("");
		LogManager logManager = new LogManager();
		String content = logManager.getData();
		logJTextArea.append(content);
	}
}
