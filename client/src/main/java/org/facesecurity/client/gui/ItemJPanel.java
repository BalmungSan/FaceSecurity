package org.facesecurity.client.gui;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.facesecurity.client.crudel.PersonCRUDEL;
import org.facesecurity.client.models.Person;


public class ItemJPanel extends JPanel implements ActionListener{
	JTextArea messageJTextArea;
	JLabel photoJLabel;
	JLabel labelJLabel;
	JButton deleteJButton;
	ListPersonJPanel parentListPerson = null;
	private Person person;
	
	public ItemJPanel(ListPersonJPanel parent, Person person){
		this.parentListPerson = parent;
		this.person = person;
		int width = 130;
		int height = 30;
		int x = 70;
		int y = 15;
		String label = "" + person.getLabel();
		setLayout(null);
		ImageIcon personIcon = new ImageIcon(person.getPhoto());
		Image personImage = personIcon.getImage();
		Image personImg = personImage.getScaledInstance(80, 120, java.awt.Image.SCALE_SMOOTH);
		personIcon = new ImageIcon(personImg);
		
		photoJLabel = new JLabel(personIcon);
		photoJLabel.setBounds(x, y, 80, 120);
		
		messageJTextArea = new JTextArea(person.getMessage());
		messageJTextArea.setBounds(180, y, width*3, height*2);
		messageJTextArea.setLineWrap(true);
		messageJTextArea.setEditable(false);
		Color bg = Color.decode("#EEEEEE");
		messageJTextArea.setBackground(bg);
		
		labelJLabel = new JLabel(label);
		labelJLabel.setBounds(30, y*4, width, height);
		
		deleteJButton = new JButton("Delete");
		deleteJButton.setBounds(x*4, y*5,width,height);
		deleteJButton.setActionCommand("Delete");
		deleteJButton.addActionListener(this);
		add(messageJTextArea);
		add(labelJLabel);
		add(photoJLabel);
		add(deleteJButton);
	}
	
	public void remove(){
		PersonCRUDEL personCRUDEL = PersonCRUDEL.getInstance();
		personCRUDEL.delete(this.person.getLabel());
		parentListPerson.remove(this);
		parentListPerson.revalidate();
		parentListPerson.repaint();
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Delete":
			remove();
			break;

		default:
			break;
		}	
	}

}
