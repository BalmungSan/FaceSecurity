package org.facesecurity.client.security;

/**
 * <h1>Cipher Exception</h1>
 * <p>Thrown by the Cipher to indicate problems while encrypting or decrypting</p>
 * <p><b>Note:</b> this class extends the standard RuntimeException class</p>
 *
 * @see Cipher
 * @see RuntimeException
 * @author Luis Miguel Mejía Suárez
 */
public class CipherException extends RuntimeException {
  /**
   * <h1>Constructor</h1>
   * <p>Constructs a CipherException with the specified detail message</p>
   * @param message the detail message
   */
  public CipherException(String message) {
    super("CipherException: " + message);
  }
}
