package org.facesecurity.client.settings;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.facesecurity.client.gui.ItemJPanel;
import org.facesecurity.client.security.Cipher;

public class OperationSocket{
	String ip;
	int operationPort;	
	SettingsManager settings;
	private Socket socket;
	private static final Logger LOGGER = Logger.getLogger(ItemJPanel.class.getName());
	private static OperationSocket instance = null;
	private final BufferedReader br;
	private final DataOutputStream out;
	
	private OperationSocket(){
		BufferedReader tempBr = null;
		DataOutputStream tempOut = null;
		settings = SettingsManager.getInstance();
		operationPort = Integer.parseInt(settings.getOperationPort());
		ip = settings.getIp();
		this.socket = new Socket();
		try {
			this.socket.connect(new InetSocketAddress(InetAddress.getByName(ip), operationPort));
			tempOut = new DataOutputStream(socket.getOutputStream());
			tempBr = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
		}catch (IOException e) {
			LOGGER.log( Level.SEVERE, e.toString(), e);
		}
		this.br = tempBr;
		this.out = tempOut;
	}
	
	public static OperationSocket getInstance(){
		if(instance == null){
			instance = new OperationSocket();
		}
		return instance;
	}
	
	
	public void send(byte[] message){
		try {
			this.out.write(message);
			this.out.writeInt('\n');
		}catch (IOException e) {
			LOGGER.log( Level.SEVERE, e.toString(), e);
		}
	}
	
	public String read(){
		String jsonMessage = "";
		try{
			String data;
			if((data = this.br.readLine()) != null){
				jsonMessage = new String(Cipher.getInstance().decrypt(data.getBytes()));
			}
		} catch (IOException e) {
			LOGGER.log( Level.SEVERE, e.toString(), e);
		}
		return jsonMessage;
	}
	
}
