package org.facesecurity.client.security;

//util imports
import java.util.Arrays;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

//security imports
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

//application imports
import org.facesecurity.client.settings.SettingsManager;

/**
 * <h1>Cipher Class</h1>
 *
 * <p>This class is designed to encrypt and decrypt messages using AES/CFB8/NoPadding algorithm</p>
 * <p>Messages between the client and the server must be encoded using Base64</p>
 * <p>Encryption is made with one private key shared between this client and the server
 * <br>The key is configurable in the GUI and is accessed by this class thorugh the SettingsManager</p>
 * <p><b>Note:</b> this class is a Singleton</p>
 *
 */
public class Cipher {
  //the unique instance of this class
  private static volatile Cipher instance = null;

  //the logger for this class
  private static final Logger LOGGER = Logger.getLogger(Cipher.class.getName());

  //AES uses 16 bytes (128 bits) block syze
  private static final int AES_BLOCK_SIZE = 16;

  //the key used for encryption
  private final Key key;

  //the real cipher
  private final javax.crypto.Cipher myCipher;
  
  /**
   * Constructor
   * Is private to guarantee only one instance
   * Initialize the key and the cipher
   */
  private Cipher(String strKey) {
  	Key tempKey = null;
  	javax.crypto.Cipher tempCipher = null; 

  	try {
      tempKey = new SecretKeySpec(strKey.getBytes(), "AES");
      tempCipher = javax.crypto.Cipher.getInstance("AES/CFB8/NoPadding");
    } catch (SecurityException | NoSuchAlgorithmException | NoSuchPaddingException ex) {
      LOGGER.log(Level.SEVERE, "Couldn't create the cipher\n\r" + ex.toString(), ex);
      System.exit(-1);
    }
  	
  	this.key = tempKey;
  	this.myCipher = tempCipher;
  }
  
  /**
   * The only way to get access to the unique instance of this class
   */
  public static synchronized Cipher getInstance() {
    if (instance == null){
      String strKey = SettingsManager.getInstance().getKey();
    	instance = new Cipher(strKey);
    }
    return instance;
  }

  /**
   * Obtain a new test instance of the cipher class
   * @param key the key to initialize the cipher
   * @return the new instance
   * @note this method is only available for the test cases
   */
  protected static Cipher getTestInstance(String key) {
    return new Cipher(key);
  }

  /**
   * <h1>Encrypts a message</h1>
   * <p>The message is encrypted using the private configured key and then is base64 encoded</p>
   * <p><b>Note:</b> this method generate a random iv to encrypt the message
   *  and then is append in the beginning of the returned message</p>
   *
   * @param message a byte array with the message to encrypt
   * @return the message ready to be sent to a server through a socket as an array of bytes
   * @throws CipherException if any error occurred during the encryption
   * @see #encrypt(String)
   * @see Base64
   */
  public synchronized byte[] encrypt (byte[] message) throws CipherException {
  	byte[] result = null;

    try {
			this.myCipher.init(javax.crypto.Cipher.ENCRYPT_MODE, this.key);
	    byte[] iv = this.myCipher.getIV();
	    byte[] enc = this.myCipher.doFinal(message);
	    byte[] data = new byte[AES_BLOCK_SIZE + enc.length];
	    for (int i = 0; i < AES_BLOCK_SIZE; i++){
	    	data[i] = iv[i];
	    }
	    for (int i = 0; i < enc.length; i++){
	    	data[i + AES_BLOCK_SIZE] = enc[i];
	    }
	    result = Base64.getEncoder().encode(data); 
		} catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
      String msg = "Error while encrypting the message\n\r" + ex.toString();
      LOGGER.log(Level.SEVERE, msg, ex);
      throw new CipherException(msg);
    }
    
    return result;
  }

  /**
   * <h1>Encrypts a message</h1>
   * <p>The message is encrypted using the private configured key and then is base64 encoded</p>
   * <p><b>Note:</b> this method generate a random iv to encrypt the message
   *  and then is append in the beginning of the returned message</p>
   *
   * @param message a String with the message to encrypt
   * @return the message ready to be sent to a server through a socket as an array of bytes
   * @throws CipherException if any error occurred during the encryption
   * @see #encrypt(byte[])
   * @see Base64
   * @see String#getBytes()
   */
  public synchronized byte[] encrypt (String message) throws CipherException {
    return this.encrypt(message.getBytes());
  }

  /**
   * <h1>Decrypt a message</h1>
   * The message is base64 decoded and then decrypted using the private configured key</p>
   * <p><b>Note:</b> this method uses the first 16 bytes of the message as the iv for decryption</p>
   * <p><b>Note:</b> to generate a string with the original message
   *  pass the bytes returned by this method to the constructor of a new string</p>
   *
   * @param enc a byte array with the message to decrypt
   * @return the original message as an array of bytes
   * @throws CipherException if any error occurred during the decryption
   * @see Base64
   */
  public synchronized byte[] decrypt (byte[] enc) throws CipherException {
    byte[] message = null;

    try {
    	byte[] data = Base64.getDecoder().decode(enc);
      IvParameterSpec iv = new IvParameterSpec(Arrays.copyOf(data, AES_BLOCK_SIZE));
			this.myCipher.init(javax.crypto.Cipher.DECRYPT_MODE, this.key, iv);
			message = Arrays.copyOfRange(this.myCipher.doFinal(data), AES_BLOCK_SIZE, data.length);
		} catch (InvalidKeyException | InvalidAlgorithmParameterException
              | IllegalBlockSizeException | BadPaddingException ex) {
      String msg = "Error while decrypting the message\n\r" + ex.toString();
      LOGGER.log(Level.SEVERE, msg, ex);
      throw new CipherException(msg);
		}
    
    return message;
  }
}
