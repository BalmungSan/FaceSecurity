package org.facesecurity.client.jsonformat;

import java.util.Base64;

import org.facesecurity.client.models.Notification;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class NotificationJSON {
	
	private JSONParser parser;
	
	public NotificationJSON(){
		parser = new JSONParser();
	}
	
	public Notification getInformation(String messageJSON) throws ParseException{
		JSONObject jsonObject = (JSONObject) parser.parse(messageJSON);
		String message = (String)jsonObject.get("message");
		int confidence = (int) ((long) jsonObject.get("confidence"));
		String date = (String) jsonObject.get("date");
		String photoDB = (String) jsonObject.get("photo_db");
		byte[] decodedBytes = Base64.getDecoder().decode(photoDB.getBytes());
		String photoCamera = (String) jsonObject.get("photo_camera");
		byte[] decodedBytes1 = Base64.getDecoder().decode(photoCamera.getBytes());
		return new Notification(message, confidence, date, decodedBytes, decodedBytes1);
	}
}
